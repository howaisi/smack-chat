//
//  GradiuntView.swift
//  Chat_App
//
//  Created by Hozaifa on 12/23/17.
//  Copyright © 2017 Hozaifa. All rights reserved.
//

import UIKit
@IBDesignable
class GradiuntView: UIView {

    @IBInspectable var topColor:UIColor = #colorLiteral(red: 0.2901960784, green: 0.3019607843, blue: 0.8470588235, alpha: 1) {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var bottomColor: UIColor = #colorLiteral(red: 0.1725490196, green: 0.831372549, blue: 0.8470588235, alpha: 1) {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        let gradiuntLayer = CAGradientLayer()
        gradiuntLayer.colors = [topColor.cgColor,bottomColor.cgColor]
        gradiuntLayer.startPoint = CGPoint(x: 0, y: 0)
        gradiuntLayer.endPoint = CGPoint(x: 1, y: 1)
        gradiuntLayer.frame = self.bounds
        self.layer.insertSublayer(gradiuntLayer, at: 0)
        
    }

}
