//
//  ChannelVC.swift
//  Chat_App
//
//  Created by Hozaifa on 12/23/17.
//  Copyright © 2017 Hozaifa. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController {
    @IBOutlet weak var loginBtn: UIButton!
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //making this size thats is shown when slided from the caht VC
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60 
    }
    
    @IBAction func loginBtnPressed(_ sender: UIButton) {
        performSegue(withIdentifier: TO_LOGIN, sender: nil)
    }
    
    

}
