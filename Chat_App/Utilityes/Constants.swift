//
//  Constants.swift
//  Chat_App
//
//  Created by Hozaifa on 12/25/17.
//  Copyright © 2017 Hozaifa. All rights reserved.
//

import Foundation

let TO_LOGIN = "toLogin"
let TO_CREATE_ACCOUNT = "toCreateAccount"
let UNWIND = "unwindToChannel"
